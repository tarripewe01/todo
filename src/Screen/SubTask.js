import React, {useState, useEffect} from 'react';
import {
  Image,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Modal,
  SafeAreaView,
  ScrollView,
  StyleSheet,
} from 'react-native';
import AddIcon from 'react-native-vector-icons/MaterialIcons';
import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

import Clip from '../Assests/Icon/Clip.png';
import {Colors} from '../Util/shared';

const Todo = ({route, navigation}) => {
  const {id, page, created, task_names} = route.params;
  const [modal, setModal] = useState(false);
  const [data, setData] = useState(null);
  const [addingTask, setaddingTask] = useState(null);

  useEffect(() => {
    console.log('params:', id, page, created, task_names);
    getTask();
  }, []);

  const modalAdd = () => {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={modal}
        onRequestClose={() => setModal(false)}>
        <SafeAreaView style={styles.container_modal}>
          <View
            style={{
              height: 360,
              backgroundColor: Colors.kWhite,
              justifyContent: 'center',
              borderRadius: 5,
              marginHorizontal: 30,
            }}>
            <View style={styles.container_title}>
              <View style={{flexDirection: 'column'}}>
                <Text style={styles.text}>New subtask</Text>
              </View>
              <AddIcon
                onPress={() => setModal(false)}
                name="close"
                size={25}
                style={styles.addIcon}
              />
            </View>
            <View style={{alignItems: 'center', marginTop: 50}}>
              <TextInput
                onChangeText={text => setaddingTask(text)}
                style={styles.container_input}
                value={addingTask}
                placeholder="Subtask name"
                placeholderTextColor={Colors.kGrey}
              />
              <TouchableOpacity
                onPress={() =>
                  addTask().then(() => {
                    getTask();
                  })
                }
                style={styles.container_btn}>
                <Text style={styles.btn_save}>SAVE</Text>
              </TouchableOpacity>
            </View>
          </View>
        </SafeAreaView>
      </Modal>
    );
  };

  const getTask = async () => {
    try {
      const token = await AsyncStorage.getItem('userToken');
      console.log('email:', token, id, page);
      Axios({
        method: 'GET',
        url: `https://task.blck.id/api/todo/${id}?page=${page}`,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }).then(res => {
        if (res) {
          console.log('res Task:', res.data);
          setData(res.data);
        }
      });
    } catch (error) {
      console.error('error', {...error});
    }
  };

  const addTask = async () => {
    try {
      const token = await AsyncStorage.getItem('userToken');
      console.log('email:', token);
      Axios({
        method: 'POST',
        url: 'https://task.blck.id/api/subtask',
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data: {
          sub_task_name: addingTask,
          todo_id: id,
        },
      }).then(res => {
        console.log('res:', res.data);
        setaddingTask(res?.data);
        setData(data);
        setModal(false);
      });
    } catch (error) {
      console.error('error', {...error});
    }
  };

  const renderTask = item => {
    return item?.todo.data.map(value => (
      <TouchableOpacity style={styles.container_list}>
        <View style={styles.list}>
          <View style={{flexDirection: 'column'}}>
            <Text style={styles.list_title}>{value.sub_task_name}</Text>
            <Text style={{fontSize: 10, marginVertical: 2}}>
              {value.created_at == value.updated_at
                ? 'Created at: ' + value.created_at.split('T')[0]
                : 'Upadated at: ' + value.updated_at.split('T')[0]}
            </Text>
          </View>
          <Image source={Clip} style={styles.clipIcon} />
        </View>
      </TouchableOpacity>
    ));
  };
  return (
    <View style={styles.container}>
      {modalAdd()}
      <View style={styles.container_text}>
        <View style={{flexDirection: 'column'}}>
          <Text style={styles.text_date}>
            Created at {created.split('T')[0]}{' '}
            {created.split('T')[1].split('.')[0]}
          </Text>
          <Text style={styles.text_title}>{task_names}</Text>
        </View>
        <TouchableOpacity onPress={() => setModal(true)}>
          <AddIcon
            name="add"
            style={styles.iconAdd}
            size={40}
            color={Colors.kWhite}
          />
        </TouchableOpacity>
      </View>
      <ScrollView style={{marginBottom: 30}}>{renderTask(data)}</ScrollView>
    </View>
  );
};
export default Todo;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.kWhite,
  },
  container_text: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 30,
    marginHorizontal: 30,
  },
  text_date: {
    fontSize: 10,
    fontWeight: 'bold',
    textAlign: 'left',
    textAlignVertical: 'center',
  },
  text_title: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    textAlignVertical: 'center',
    color: Colors.kPrimary,
  },
  iconAdd: {
    height: 40,
    width: 40,
    borderRadius: 5,
    alignSelf: 'center',
    backgroundColor: Colors.kPrimary,
  },
  container_modal: {
    flex: 1,
    backgroundColor: Colors.kBlack,
    opacity: 0.9,
    justifyContent: 'center',
  },
  container_title: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    marginHorizontal: 15,
    marginTop: 0,
  },
  text: {
    fontWeight: 'bold',
    fontSize: 16,
    color: Colors.kPrimary,
  },
  addIcon: {
    height: 50,
    width: 40,
    borderRadius: 5,
    alignSelf: 'center',
  },
  container_input: {
    width: '80%',
    borderBottomWidth: 1,
    alignSelf: 'center',
    color: Colors.kBlack,
  },
  container_btn: {
    height: 40,
    width: '80%',
    marginTop: 130,
    alignSelf: 'center',
    backgroundColor: Colors.kPrimary,
  },
  btn_save: {
    height: 40,
    color: Colors.kWhite,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  container_list: {
    width: '85%',
    justifyContent: 'center',
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: Colors.kPrimary,
    borderRadius: 5,
    marginVertical: 10,
  },
  list: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 15,
    paddingVertical: 10,
  },
  list_title: {
    fontSize: 12,
    fontWeight: 'bold',
    marginVertical: 2,
    color: Colors.kPrimary,
  },
  clipIcon: {
    height: 50,
    width: 40,
    borderRadius: 5,
    alignSelf: 'center',
  },
});
