
import React, {useState, useEffect} from 'react';
import {
  Image,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Modal,
  SafeAreaView,
  ScrollView,
  StyleSheet,
} from 'react-native';
import AddIcon from 'react-native-vector-icons/MaterialIcons';
import Axios from 'axios';

import Clip from '../Assests/Icon/Clip.png';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Colors} from '../Util/shared';

const Todo = ({navigation}) => {
  const [modal, setModal] = useState(false);
  const [data, setData] = useState(null);
  const [todo, setTodo] = useState(null);
  const [addingTodo, setaddingTodo] = useState(null);

  useEffect(() => {
    getTodo();
  }, []);

  const modalAdd = () => {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={modal}
        onRequestClose={() => setModal(false)}>
        <SafeAreaView style={styles.modal}>
          <View style={styles.container_modal}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'flex-start',
                justifyContent: 'space-between',
                marginHorizontal: 15,
                marginTop: 0,
              }}>
              <View style={{flexDirection: 'column'}}>
                <Text style={styles.text}>New todo</Text>
              </View>
              <AddIcon
                onPress={() => setModal(false)}
                name="close"
                size={25}
                style={{
                  height: 50,
                  width: 40,
                  borderRadius: 5,
                  alignSelf: 'center',
                }}
              />
            </View>
            <View style={{alignItems: 'center', marginTop: 50}}>
              <TextInput
                onChangeText={text => setaddingTodo(text)}
                style={{
                  width: '80%',
                  borderBottomWidth: 1,
                  alignSelf: 'center',
                  color: Colors.kBlack,
                }}
                value={addingTodo}
                placeholder="Todo name"
                placeholderTextColor={Colors.kGrey}
              />
              <TouchableOpacity
                onPress={() =>
                  addTodo().then(() => {
                    getTodo();
                  })
                }
                style={styles.container_btn}>
                <Text
                  style={styles.btn_save}>
                  SAVE
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </SafeAreaView>
      </Modal>
    );
  };

  const getTodo = async () => {
    try {
      const token = await AsyncStorage.getItem('userToken');
      console.log('email:', token);
      Axios({
        method: 'GET',
        url: 'https://task.blck.id/api/todo',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }).then(res => {
        console.log('res:', res.data);
        setData(res?.data);
      });
    } catch (error) {
      console.error('error', {...error});
    }
  };

  const addTodo = async () => {
    try {
      const token = await AsyncStorage.getItem('userToken');
      console.log('email:', token);
      Axios({
        method: 'POST',
        url: 'https://task.blck.id/api/todo',
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data: {
          task_name: addingTodo,
        },
      }).then(res => {
        console.log('res:', res.data);
        setTodo(res?.data);
        setData(data);
        setModal(false);
      });
    } catch (error) {
      console.error('error', {...error});
    }
  };

  const renderTodo = item => {
    console.log('item:', item);
    return item?.data.map((value, id) => (
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('SubTask Screen', {
            id: value.id,
            page: item.from,
            created: value.created_at,
            task_names: value.task_name,
          })
        }
        style={styles.container_list}>
        {console.log('value:', value)}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginHorizontal: 15,
            paddingVertical: 10,
          }}>
          <View style={{flexDirection: 'column'}}>
            <Text style={styles.list}>{value.task_name}</Text>
            <Text style={{fontSize: 10, marginVertical: 2}}>
              {value.created_at == value.updated_at
                ? 'Created at: ' + value.created_at.split('T')[0]
                : 'Upadated at: ' + value.updated_at.split('T')[0]}
            </Text>
          </View>
          <Image source={Clip} style={styles.clipIcon} />
        </View>
      </TouchableOpacity>
    ));
  };

  return (
    <View style={styles.container}>
      <View style={styles.container_title}>
        <Text style={styles.textTitle}>TODO</Text>
        <TouchableOpacity onPress={() => setModal(true)}>
          <AddIcon
            name="add"
            style={styles.addIcon}
            size={40}
            color={Colors.kWhite}
          />
        </TouchableOpacity>
      </View>
      {modal ? modalAdd() : null}
      <ScrollView style={{marginBottom: 30}}>{renderTodo(data)}</ScrollView>
    </View>
  );
};
export default Todo;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.kWhite,
  },
  container_title: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 30,
    marginHorizontal: 30,
    fontFamily: 'roboto'
  },
  textTitle: {
    fontSize: 30,
    fontWeight: 'bold',
    alignSelf: 'center',
    textAlign: 'left',
    textAlignVertical: 'center',
    color: Colors.kPrimary,
  },
  addIcon: {
    height: 40,
    width: 40,
    borderRadius: 5,
    alignSelf: 'center',
    backgroundColor: Colors.kPrimary,
  },
  container_list: {
    width: '85%',
    justifyContent: 'center',
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: Colors.kPrimary,
    borderRadius: 5,
    marginVertical: 10,
  },
  list: {
    fontSize: 12,
    fontWeight: 'bold',
    marginVertical: 2,
    color: Colors.kPrimary,
  },
  clipIcon: {
    height: 50,
    width: 40,
    borderRadius: 5,
    alignSelf: 'center',
  },
  modal: {
    flex: 1,
    backgroundColor: Colors.kBlack,
    opacity: 0.9,
    justifyContent: 'center',
  },
  container_modal: {
    height: 360,
    backgroundColor: Colors.kWhite,
    justifyContent: 'center',
    borderRadius: 5,
    marginHorizontal: 30,
  },
  text: {
    fontWeight: 'bold',
    fontSize: 16,
    color: Colors.kPrimary,
  },
  container_btn:{
    height: 40,
    width: '80%',
    marginTop: 130,
    alignSelf: 'center',
    backgroundColor: Colors.kPrimary,
  },
  btn_save: {
    height: 40,
    color: Colors.kWhite,
    textAlign: 'center',
    textAlignVertical: 'center',
  }
});
