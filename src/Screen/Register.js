import { useNavigation } from '@react-navigation/native';
import axios from 'axios';
import React, { useState, useEffect } from 'react';
import {
    Image,
    ActivityIndicator,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    Alert,
    StyleSheet
} from 'react-native';
import { Colors } from '../Util/shared';

const Register = () => {
    const navigation = useNavigation();
    const [loading, setLoading] = useState(false);
    const [user_Name, setUser_Name] = useState('');
    const [user_Email, setUser_Email] = useState('');
    const [user_Password, setUser_Password] = useState('');
    const [user_Confirmpassword, setUser_Confirmpassword] = useState('');


    const handleRegister = async () => {
        try {
            console.log("try:", user_Name, user_Email, user_Password, user_Confirmpassword)
            Axios({
                method: 'POST',
                url: "https://task.blck.id/api/register",
                headers: {
                    "Accept": "application/json"
                },
                data: {
                    name: user_Name,
                    email: user_Email,
                    password: user_Password,
                    password_confirmation: user_Confirmpassword,
                }
            }).then(res => {
                if (res) {
                    console.log('result:', res)
                    Alert.alert("Registration Successful")
                }
            })
        } catch (error) {
            console.error("error", { ...error });
            // setLoading(false);
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.container_imageTop}>
                <View
                    style={styles.image_top}>
                    <Text style={{ color: '#07758D' }}>.</Text>
                </View>
            </View>
            <View style={styles.container_register}>
                <Text
                    style={styles.text_register}>REGISTER</Text>
                <TextInput
                    style={styles.form}
                    placeholder='Username'
                    placeholderTextColor={Colors.kGrey}
                    onChangeText={text => setUser_Name(text)}
                    value={user_Name}
                />
                <TextInput
                    style={styles.form}
                    autoCapitalize='none'
                    placeholder='E-mail'
                    placeholderTextColor={Colors.kGrey}
                    onChangeText={text => setUser_Email(text)}
                    value={user_Email}
                />
                <TextInput
                    style={styles.form}
                    autoCapitalize='none'
                    placeholder='Password'
                    placeholderTextColor={Colors.kGrey}
                    secureTextEntry={true}
                    onChangeText={text => setUser_Password(text)}
                    value={user_Password}
                />
                <TextInput
                    style={styles.form}
                    autoCapitalize='none'
                    placeholder='Password'
                    placeholderTextColor={Colors.kGrey}
                    secureTextEntry={true}
                    onChangeText={text => setUser_Confirmpassword(text)}
                    value={user_Confirmpassword}
                />
                <TouchableOpacity
                    onPress={() => handleRegister()}
                    style={styles.container_btn_register}>
                    <Text
                        style={styles.btn_register}>REGISTER</Text>
                </TouchableOpacity>
                <Text
                    onPress={() => navigation.navigate("Login Screen")}
                    style={styles.btn_login}>Login now.</Text>
            </View>
            <View style={styles.container_imageBottom}>
                <View
                    style={styles.image_bottom}>
                    <Text style={{ color: Colors.kPrimary }}>.</Text>
                </View>
            </View>
        </View>
    );
};
export default Register;

const styles = StyleSheet.create({
  container: { 
    flex: 1, 
    backgroundColor: Colors.kWhite
  },
  container_imageTop: { 
    flexDirection: 'row', 
    justifyContent: 'flex-end' 
  },
  image_top: {
    height: 100,
    width: 100,
    borderBottomLeftRadius: 100,
    alignSelf: 'flex-end',
    backgroundColor: Colors.kPrimary
  },
  container_register: { 
    flex: 1, 
    justifyContent: 'center' 
  },
  text_register: {
    height: 40,
    width: "80%",
    marginTop: 30,
    marginBottom: 39,
    fontSize: 30,
    fontWeight: 'bold',
    alignSelf: 'center',
    textAlign: 'left',
    textAlignVertical: 'center',
    color: Colors.kPrimary,
  },  
  form: {
    width: '80%',
    borderBottomWidth: 1,
    alignSelf: 'center',
    color: '#000'
 },
 container_btn_register: {
    height: 40,
    width: '80%',
    marginTop: 50,
    alignSelf: 'center',
    backgroundColor: Colors.kPrimary
  },
  btn_register: {
    height: 40,
    color: Colors.kWhite,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  btn_login: {
    height: 40,
    width: "80%",
    marginTop: 30,
    alignSelf: 'center',
    textAlign: 'right',
    textAlignVertical: 'center',
    textDecorationLine: 'underline',
    color: Colors.kPrimary,
  },
  container_imageBottom: { 
    flexDirection: 'row', 
    justifyContent: 'flex-start' 
  },
  image_bottom: {
    height: 100,
    width: 100,
    borderTopRightRadius: 100,
    alignSelf: 'flex-start',
    backgroundColor: Colors.kPrimary
  }
})
