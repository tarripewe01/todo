import AsyncStorage from '@react-native-async-storage/async-storage';
import {useNavigation} from '@react-navigation/native';
import React, {useState, useEffect} from 'react';
import {
  Image,
  ActivityIndicator,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Axios from 'axios';
import Border from '../Assests/Icon/Border.png';
import { Colors } from '../Util/shared';

const Login = () => {
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);
  const [user_Email, setUser_Email] = useState('');
  const [user_Password, setUser_Password] = useState('');

  const handleLogin = async () => {
    try {
      console.log('email:', user_Email, user_Password);
      Axios({
        method: 'POST',
        url: 'https://task.blck.id/oauth/token',
        headers: {
          Accept: 'application/json',
        },
        data: {
          client_secret: 'WUiAA8Mvr4yHX6bfZuPo4jiM3hdFwZk3TeDubgpo',
          client_id: '2',
          username: user_Email,
          password: user_Password,
          grant_type: 'password',
        },
      }).then(res => {
        if (res) {
          AsyncStorage.setItem('userToken', res.data.access_token);
          navigation.navigate('Todo Screen');
          // const result = res.data;
          console.log('result:', res.data);
        }
      });
    } catch (error) {
      console.error('error', {...error});
      setLoading(false);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.container_image}>
        <Image
          source={Border}
          style={styles.image}
        />
      </View>
      <View style={styles.container_text}>
        <Text
          style={styles.text_login}>
          LOGIN
        </Text>
        <TextInput
          onChangeText={text => setUser_Email(text)}
          value={user_Email}
          style={styles.form_email}
          autoCapitalize="none"
          placeholder="Username"
          placeholderTextColor={Colors.kGrey}
        />
        <TextInput
          onChangeText={text => setUser_Password(text)}
          value={user_Password}
          style={styles.form_password}
          autoCapitalize="none"
          placeholder="Password"
          placeholderTextColor={Colors.kGrey}
          secureTextEntry={true}
        />
        <TouchableOpacity
          onPress={() => handleLogin()}
          style={styles.container_btn_login}>
          <Text
            style={styles.btn_login}>
            LOGIN
          </Text>
        </TouchableOpacity>
        <Text
          onPress={() => navigation.navigate('Register Screen')}
          style={styles.btn_register}>
          Create new account.
        </Text>
      </View>
      <View style={styles.container_image2}>
        <Image
          source={Border}
          style={styles.image2}
        />
      </View>
    </View>
  );
};
export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.kWhite,
  },
  container_image: {
    flexDirection: 'row', 
    justifyContent: 'flex-end'
  },
  image: {
    height: 100, 
    width: 100, 
    resizeMode: 'stretch'
  },
  container_text: {
    flex: 1, 
    justifyContent: 'center'
  },
  text_login: {
    height: 40,
    width: '80%',
    marginTop: 30,
    marginBottom: 39,
    fontSize: 30,
    fontWeight: 'bold',
    alignSelf: 'center',
    textAlign: 'left',
    textAlignVertical: 'center',
    color: Colors.kPrimary,
  },
  form_email: {
    width: '80%',
    borderBottomWidth: 1,
    alignSelf: 'center',
    color: Colors.kBlack,
  },
  form_password: {
    width: '80%',
    borderBottomWidth: 1,
    alignSelf: 'center',
    color: Colors.kBlack,
  },
  container_btn_login: {
    height: 40,
    width: '80%',
    marginTop: 50,
    alignSelf: 'center',
    backgroundColor: Colors.kPrimary,
  },
  btn_login: {
    height: 40,
    color: Colors.kWhite,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  btn_register: {
    height: 40,
    width: '80%',
    marginTop: 30,
    alignSelf: 'center',
    textAlign: 'right',
    textAlignVertical: 'center',
    textDecorationLine: 'underline',
    color: Colors.kPrimary,
  },
  container_image2: {
    flexDirection: 'row', 
    justifyContent: 'flex-start'
  },
  image2: {
    height: 100,
    width: 100,
    resizeMode: 'stretch',
    transform: [{rotateZ: '180deg'}],
  }
});
