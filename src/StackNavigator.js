import React, { useState } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from './Screen/Login';
import RegisterScreen from './Screen/Register/';
import TodoScreen from './Screen/Todo/';
import SubTaskScreen from './Screen/SubTask/';


const Stack = createStackNavigator();

const StackNavigator = ({ navigation }) => {
    const [initialRoute, setRoute] = useState("Login Screen");

    return (
        <Stack.Navigator initialRouteName={initialRoute} >
            <Stack.Screen
                name="Login Screen"
                component={LoginScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="Register Screen"
                component={RegisterScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="Todo Screen"
                component={TodoScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="SubTask Screen"
                component={SubTaskScreen}
                options={{ headerShown: false }}
            />
        </Stack.Navigator>
    );
};
export default StackNavigator;
